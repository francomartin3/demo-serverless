import boto3
client = boto3.client('dynamodb')
userid = "cf6b22b6-3cc7-49be-a4a6-dab17d1a0b3c"
response = client.query(
    TableName='tmp-franco-std',
    Select='ALL_ATTRIBUTES',
    KeyConditions={
        'pk': {
            'AttributeValueList': [{
                    'S': f"user-{userid}",
                    }
                ],
            'ComparisonOperator': 'EQ'
            },
        }
)
print(response)
user_name = ""
for item in response['Items']:
    if item['sk']['S'] == 'metadata':
        user_name = item['name']['S']
        break
orders = []
for item in response['Items']:
    if item['sk']['S'] == 'orders':
        # List Comprehension
        # orders = [{order['orderid']['S']: order['data']['S']} for order in response['Items']]
        orders = []
        for order_id, order_data in item['orders']['M'].items():
            orders.append({
                order_id: order_data['S']
            })
        break
#import json
#print(json.dumps({"name": user_name, "orders": orders}, indent=2))
###
{
  "name": "franco martin",
  "orders": [
    {
      "fbfbb112-2025-42cc-ab5e-121d64741357": "algo-sobre-la-orden"
    }
  ]
}
###