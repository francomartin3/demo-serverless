import boto3
client = boto3.client('dynamodb')
userid = "cf6b22b6-3cc7-49be-a4a6-dab17d1a0b3c"
response = client.get_item(
    TableName='tmp-franco-users',
    Key={
        'userid': {
            'S': userid,
        }
    }
)
user_name = response['Item']['name']['S']
response = client.scan(
    TableName='tmp-franco-orders',
    Limit=5,
    Select='ALL_ATTRIBUTES',
    ScanFilter={
        'userid': {
            'AttributeValueList': [
                {
                    'S': userid
                },
            ],
            'ComparisonOperator': 'EQ'
        }
    }
)
# List Comprehension
# orders = [{order['orderid']['S']: order['data']['S']} for order in response['Items']]
orders = []
for order in response['Items']:
    order_id = order['orderid']['S']
    order_data = order['data']['S']
    orders.append({
        order_id: order_data 
    })

#import json
#print(json.dumps({'name': user_name, 'orders': orders}, indent=2))
###
{
  "name": "franco martin",
  "orders": [
    {
      "fbfbb112-2025-42cc-ab5e-121d64741357": "algo-sobre-la-orden"
    }
  ]
}
###
