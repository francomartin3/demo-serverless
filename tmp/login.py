import boto3
import getpass
password = getpass.getpass("Give me a password: ")
client = boto3.client('cognito-idp')
client_identity = boto3.client('cognito-identity')

user_pools = client.list_user_pools(
    MaxResults=60
)
pool_id = ""
for pool in user_pools['UserPools']:
    if pool['Name'] == "serverless-test":
        pool_id = pool['Id']
print(f"Pool id is {pool_id}")
response = client.admin_set_user_password(
    UserPoolId=pool_id,
    Username='admin',
    Password=password,
    Permanent=True
)
if response['ResponseMetadata']['HTTPStatusCode'] == 200:
    print("Password reset ok!")
else:
    print("Password reset failed")
    print(response)
    exit(1)
response = client.list_user_pool_clients(
    UserPoolId=pool_id,
    MaxResults=60)
client_id = response['UserPoolClients'][0]['ClientId']
resp = client.initiate_auth(
    #UserPoolId=pool_id,
    ClientId=client_id,
    AuthFlow='USER_PASSWORD_AUTH',
    AuthParameters={
            "USERNAME": "admin",
            "PASSWORD": password
        }
)
print(f"Here's your token:\n{resp['AuthenticationResult']['AccessToken']}")