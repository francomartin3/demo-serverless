import json
import base64
import uuid
import boto3
from boto3.dynamodb.types import TypeDeserializer, TypeSerializer
import os

# Transforma tipos de dato de python en tipos de dato de dynamodb
serializer = TypeSerializer()

# Transforma tipos de dato de dynamodb en tipos de dato de dynamodb
deserializer = TypeDeserializer()

# Me lo chorie de https://medium.com/towards-aws/making-use-of-boto3-out-of-the-box-dynamodb-serializers-1dffbc7deafe
def python_obj_to_dynamo_obj(python_obj: dict) -> dict:
    return {
        k: serializer.serialize(v)
        for k, v in python_obj.items()
    }

client = boto3.client('dynamodb')
def create_package(event, context):
    print(event)
    data_as_dict = json.loads(event['body'])
    
    order_id = event['pathParameters']['id']
    package_id = f"package-{uuid.uuid4()}"
    data_as_dict["pk"] = f"{order_id}"
    data_as_dict["sk"] = package_id
    print(data_as_dict)
    response = client.put_item(
        TableName=os.getenv("DDB_TABLE_NAME"),
        Item=python_obj_to_dynamo_obj(data_as_dict))
    body = {
        "message": f"Created package with id {str(package_id)} for order {str(order_id)}"
    }
    response = {"statusCode": 200, "body": json.dumps(body, indent=2)}
    return response
