import json
import base64
import uuid
import boto3
import os
from boto3.dynamodb.types import TypeDeserializer, TypeSerializer

client = boto3.client('dynamodb')

# Transforma tipos de dato de python en tipos de dato de dynamodb
serializer = TypeSerializer()

# Transforma tipos de dato de dynamodb en tipos de dato de dynamodb
deserializer = TypeDeserializer()

def dynamo_obj_to_python_obj(dynamo_obj: dict) -> dict:
    deserializer = TypeDeserializer()
    return {
        k: deserializer.deserialize(v) 
        for k, v in dynamo_obj.items()
    }  

def get_order(event, context):
    print(event)
    pk = event['pathParameters']['id']
    sk = "metadata"
    print(f"Requesting item with pk {pk} and sk {sk}")
    response = client.get_item(
        TableName=os.getenv("DDB_TABLE_NAME"),
        Key={
            'pk': {
                'S': pk,
            },
            'sk': {
                'S': sk
            }
        }
    )
    print(response)
    body = {
        "data": dynamo_obj_to_python_obj(response['Item'])
    }
    response = {"statusCode": 200, "body": json.dumps(body, indent=2)}
    return response